<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class FrontendController extends AbstractController
{

    /**
     * @Route("/", name="main")
     */
    public function main()
    {

        return $this->render('Frontend/main.html.twig', []);

    }

    /**
     * @Route("/article/{id}", name="showArticle")
     */
    public function showArticle($id) : Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article      = $entityManager->getRepository('App\Entity\Article')->findOneBy(['id'=>$id]);

        return $this->render('Frontend/showArticle.html.twig', ['article' => $article]);

    }

    /**
     * @Route("/articles", name="showArticles")
     */
    public function showArticles() : Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $articles      = $entityManager->getRepository('App\Entity\Article')->findAll();

        return $this->render('Frontend/showArticles.html.twig', ['articles' => $articles]);

    }


}