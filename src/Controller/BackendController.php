<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use App\Form\CategoryType;

/**
 * @Route("/backend")
 */
class BackendController extends AbstractController
{

    /**
     * @Route("/article/add", name="addArticle")
     */
    public function addArticle(Request $request) : Response
    {
        $entityManager     = $this->getDoctrine()->getManager();
        $article           = new Article();
        $form              = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('main', []);
        }

        return $this->render('Backend/articleForm.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/article/{id}/edit/", name="editArticle")
     */
    public function editArticle(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article       = $entityManager->getRepository('App\Entity\Article')->findOneBy(['id'=>$id]);
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('main', []);
        }

        return $this->render('Backend/articleForm.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/category/add", name="addCategory")
     */
    public function addCategory(Request $request) : Response
    {
        $entityManager     = $this->getDoctrine()->getManager();
        $category          = new Category();
        $form              = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('main', []);
        }

        return $this->render('Backend/categoryForm.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/category/{id}/edit/", name="editCategory")
     */
    public function editCategory(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $category      = $entityManager->getRepository('App\Entity\Category')->findOneBy(['id'=>$id]);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('main', []);
        }

        return $this->render('Backend/categoryForm.html.twig', ['form' => $form->createView()]);

    }

}