var Encore = require('@symfony/webpack-encore');


Encore

    .setOutputPath('public/build/')
    .setPublicPath('/build')

    .addEntry('app', './assets/js/app.js')
    .addEntry('productIngredients', './assets/js/productIngredients.js')
    .addStyleEntry('style', './assets/scss/app.scss')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps()

    .enableVersioning()

    // uncomment if you use Sass/SCSS files
    .enableSassLoader()

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()

    .enableReactPreset()
;

module.exports = Encore.getWebpackConfig();