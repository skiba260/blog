var $collectionHolder;

var $addProductIngredientButton = $('<button type="button" class="add_tag_link btn btn-primary">Add ingredient</button>');
var $newLinkLi = $('<li></li>').append($addProductIngredientButton);

jQuery(document).ready(function() {
    $collectionHolder = $('ul.productIngredients');
    $collectionHolder.append($newLinkLi);
    $collectionHolder.data('index', $collectionHolder.find(':input').length);
    $addProductIngredientButton.on('click', function(e) {
        addProductIngredientForm($collectionHolder, $newLinkLi);
    });
});

function addProductIngredientForm($collectionHolder, $newLinkLi) {
    var prototype = $collectionHolder.data('prototype');
    var index     = $collectionHolder.data('index');
    var newForm   = prototype;
    newForm       = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);
    var $newFormLi = $('<li><p class="font-weight-bold">Ingredient:</p></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}